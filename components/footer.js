import {version} from '../package.json'
import styles from '../styles/Home.module.css'

export default function Footer(props) {
    return       <footer className={styles.footer}>
    Version {version} {' '}
    <div>
    Bugs? <a href="https://gitlab.com/ozten/mastodon_bulk_find_follow/issues">Create an issue in Gitlab</a></div>

      <div>Written by <a rel="me" href="https://mastodon.technology/@ozten">ozten</a> and
      Powered by{' '} CommotionMade</div>

  </footer>;
}