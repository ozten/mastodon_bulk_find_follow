
export default function Progress(props) {
    let idx = props.idx;
    let percentageCompleted = Math.round((idx / props.total) * 100) + ' %';
    return <div style={{display: 'inline-block', width: 250, height: 245, margin: 5, padding: 10, paddingTop: 60, paddingLeft: 20, border: 'solid 1px #000'}}>
      <p>Search in progress: <bold>{idx + 1}</bold> / {props.total}</p>
      <p><span style={{fontSize: '24px'}}> {percentageCompleted}</span> complete.</p>
    </div>;
}