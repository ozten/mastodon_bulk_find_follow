export default function Profile(props) {
    let target = String(props.id);
    return <div style={{
        float: 'left', width: 250, margin: 2, borderRadius: 2,
        backgroundImage: 'url(' + props.header_static + ')', backgroundSize: 300, backgroundRepeat: 'no-repeat'
    }}>
        <a href={props.url} target={target}>
            <img style={{width: '40%', borderRadius: '50%', border: 'solid 1px #ebdc98', boxShadow: '1px 1px 1px #000',
        margin: 10}} src={props.avatar_static} alt={'The avatar of ' + props.display_name} />
        </a>
        <div style={{minHeight: 135, backgroundColor: '#fff', color: '#0e2333', padding: 10,
                     borderBottomLeftRadius: 2, borderBottomRightRadius: 2}}>
            <div>
                <bold style={{overflow: 'hidden', width: 100, color: '#0e2333'}}>{props.display_name}</bold> ({props.username})
            </div>

            <a href={props.url} target={target} style={{fontSize: 12}}>{props.url}</a><span title="Opens in a new tab" style={{border: 'solid 1px #AAA', padding: 2}}>🡕</span>
            <div style={{fontSize: 12}}>Followers: <bold style={{color: '#0e2333'}}>{props.followers_count}</bold>{' '}
                Following: {props.following_count}</div>
        <div style={{fontSize: 12}}>{/* Account created {props.created_at} and */}Last update: {props.last_status_at}</div>
        </div>
    </div>;
}