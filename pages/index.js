import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'

import Footer from '../components/footer'
import Profile from '../components/profile'
import Progress from '../components/progress'

import styles from '../styles/Home.module.css'

function searchUsername(username, cb) {
  var payload = {
    usernameList: [username]
  };
  fetch('/api/search', {
    method: 'POST',
    body: JSON.stringify(payload)
  }).then(response => response.json())
    .then(data => {

      cb(data)
    });
}
// lmorchard, jrconlin, andr00
function doIterativeSearch(usernameList, setSearchResults, setIsSearching, setIdx, setTotalSearch) {
  // TODO: show a progress bar on the search
  let usernames = usernameList.split(',').map((username) => username.trim());
  let idx = -1;
  setTotalSearch(usernames.length);

  (function _search() {
    idx++;
    setIdx(idx);
    if (idx < usernames.length) {
      let username = usernames[idx];
      searchUsername(username, (data) => {
        // TODO properly merge state
        setSearchResults((prevState, props) => {
          let results = {};
          results.accounts = [].concat(prevState.accounts || [], data.accounts);
          results.statuses = [].concat(prevState.statuses || [], data.statuses);
          results.hashtags = [].concat(prevState.hashtags || [], data.hashtags);
          return results
        });
        setTimeout(_search, 1000);
      })
    } else {
      setIsSearching(false);

    }
  })();


}

export default function Home() {
  let [isSearching, setIsSearching] = useState(false);
  let [hasSearched, setHasSearching] = useState(false);
  let [usernameList, setUsernameList] = useState('');
  let [searchResults, setSearchResults] = useState({});
  let [idx, setIdx] = useState(0);
  let [totalSearch, setTotalSearch] = useState(0);

  let handleUsernameListChange = (event) => {
    setUsernameList(event.target.value);
  }

  let disabled = isSearching ? {disabled:true} : {};

  let handleSubmit = (event) => {
    event.preventDefault();
    setIsSearching(true);
    // TODO: [X] Get a basic search working
    // https://github.com/hylyh/node-mastodon
    // https://github.com/vanita5/mastodon-api
    // TODO: [X] search for a username https://docs.joinmastodon.org/methods/search/
    // TODO: [X] move to setInterval and step through the list of usernames client-side
    // TODO: [X] show progress bar
    // TODO: [ ] Make it purdy, maybe like mastodon.technology
    // TODO: [ ] Profile lets you - hide, follow, open in new tab
    // TODO: [ ] Automate following an account https://docs.joinmastodon.org/methods/accounts/#check-relationships-to-other-accounts (Perform actions on an account) (Follow)
    /* TODO: [ ] Rate limit the API
      'x-ratelimit-limit': '300',
      'x-ratelimit-remaining': '298',
      'x-ratelimit-reset': '2022-04-26T01:10:00.912367Z', */
    // TODO: Allow O-Auth so rate-limit is per user

    doIterativeSearch(usernameList, setSearchResults, setIsSearching, setIdx, setTotalSearch);
  }

  useEffect(() => {
    if (isSearching) {
      setHasSearching(true);
    }
  }, [isSearching]);

  let profiles = [];
  if (!!searchResults && !!searchResults.accounts && !!searchResults.accounts.length > 0) {
    profiles = searchResults.accounts.map((account) => {
      return <Profile key={account.username + account.url } {...account} />
    });
  }
  let progress = '';
  if (isSearching) {
    progress = <Progress idx={idx} total={totalSearch} />
  } else if (hasSearched && searchResults.accounts.length == 0) {
    progress = <div style={{display: 'inline-block', width: 250, height: 245, margin: 5, padding: 10, paddingTop: 60, paddingLeft: 20, border: 'solid 1px #000', color: '#fff'}}>
      <h2>No Profiles Found</h2>
<p>Sorry, no results matched</p>
    </div>
  }

  let profilesAndProgress = '';
  if (profiles.length > 0 || progress != '') {
    profilesAndProgress = <div style={{backgroundColor: '#0e2333', color: '#fff', padding: 10, borderRadius: 5,
                                       display: 'flex', flexWrap: 'wrap', justifyContent: 'center', marginTop: 50}}>
      {profiles}
      {progress}
    </div>
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Mastodon Bulk Find and Follow - CommotionMade</title>
        <meta name="description" content="Mastodon Bulk Find and Follow makes it easy to migrate from Twitter to Mastodon by CommotionMade" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Bulk Find Mastodon Users</h1>
        <p>Migrating from one social network to another? We will help you check to see which of your peeps are on Mastodon (aka the Fediverse).</p>
        <div style={{backgroundColor: '#0e2333', color: '#fff', padding: 10, borderRadius: 5}}>
          <h2>How it works</h2>
          <ol style={{textAlign: 'left', lineHeight: '32px'}}>
            <li>Gather a list of usernames you want to check. For example the Twitter follows list off of your profile. {' '}
              <Link href="/how_do_i_export_twitter_followers"><a>Help! How do I export Twitter followers?</a></Link></li>
            <li>Format the list as a comma separated list of usernames with no @ symbol.<br />
            <strong>Example:</strong>{' '} If I had the friends @bob, @alice, and @janet then I would format that as<br />
            <pre style={{width: '80%'}}>bob, alice, janet</pre></li>
            <li>Paste your list into the search box below and click Search</li>
            <li>It can take a while, feel free to click on profiles as they load and they will open in a new tab.</li>
          </ol>
        </div>
        <h2>Bulk Search Text Field</h2>
        <form onSubmit={handleSubmit}>
          <div>
            <textarea onChange={handleUsernameListChange} value={usernameList} {...disabled} cols="80" rows="4"></textarea>
          </div>
          <input type="submit" {...disabled} value="Search Mastodon" />
        </form>

        {profilesAndProgress}
      </main>
      <Footer />

    </div>
  )
}
