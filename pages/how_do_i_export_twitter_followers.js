import Head from 'next/head'
import Link from 'next/link'

import styles from '../styles/Home.module.css'

import Footer from '../components/footer'

export default function TwitterExport() {
  return <div className={styles.container}>
  <Head>
    <title>How do I export Twitter followers and following usernames - Mastodon Bulk Find and Follow - CommotionMade</title>
    <meta name="description" content="Detailed instructions on how to export Twitter followers and following username" />
    <link rel="icon" href="/favicon.ico" />
  </Head>

  <main className={styles.main}>
      <h1>How do I export my Twitter followers and following username contact list?</h1>
      <p>Good question! The following are possible ways to do it...</p>
      <h2>Export Contacts using twtdata</h2>
      <p>I am not affiliated with twtdata, but it is free, doesn&apos;t require any sign in, is very simple, and worked well for me.</p>
      <ul>
          <li>Go to <a href="https://www.twtdata.com" nofollow="true">https://www.twtdata.com</a></li>
          <li>Click &lsquo;FRIENDS/FOLLOWING&rsquo;</li>
          <li>Enter your Twitter handle</li>
          <li>Enter your email address</li>
          <li>Click the button</li>
          <li>Wait a while and then check your email.</li>
          <li>Download the CSV zip file</li>
          <li>Unzip</li>
          <li>Import into a spreadsheet program</li>
          <li>Select all values in the username column and paste those into <Link href="/"><a>this tool</a></Link>. Make sure they are separated by commas</li>
</ul>
    <h2>Other Options</h2>
    <ul>
          <li>Export your Twitter data (takes several days if it works at all)</li>
          <li>Please give us suggestions via <a href="https://gitlab.com/ozten/mastodon_bulk_find_follow/issues">Gitlab issues</a></li>
      </ul>
      <p>All set? Back to the <Link href="/"><a>Mastodon Bulk Find and Follow</a></Link> page.</p>
    <div>

    </div>
  </main>
  <Footer />
  </div>
}
