var Masto = require('mastodon')

var M = new Masto({
  access_token: process.env.MASTODON_ACCESS_TOKEN,
  timeout_ms: 60*1000,  // optional HTTP request timeout to apply to all requests.
  api_url: 'https://mastodon.technology/api/v2/', // optional, defaults to https://mastodon.social/api/v1/
})

// For now, do one username at a time and throttle client side, so that we can easily
// show progress and give results as they appear
export default function handler(req, res) {
  let usernames = [];
  // We expect { usernameList: ['foo', 'bar']}
  const payload = JSON.parse(req.body);

  for (var i=0; i < payload.usernameList.length; i++) {
    let username = payload.usernameList[i];
    usernames.push(username);
    console.log('Searching: ', username);
    // TODO: type: accounts
    M.get('search',{
      q: username
    }, (err, data, response) => {
      console.log(response.headers);
      if (!!err) {
        res.status(500).json({error: err});
      } else {
        try {
          console.log('Found', data.accounts.length, 'profiles');
        } catch (err) {

        }

        res.status(200).json(data);
      }
    })
    break;
  }
}
