# Mastodon Bulk Find and Follow

As a Twitter refugee, I wanted to search all of my Twitter following and followers usernames to see if they are on Mastodon.

You can use this tool at https://commotionmade.com

Or you can run this locally with your own Mastodon API credentials. It is a Next.js site.